import os
import json
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_service(host):
    vault = host.service('vault')
    assert vault.is_enabled
    assert vault.is_running


def test_vault_cli(host):
    cmd = host.run("vault status -tls-skip-verify -format=json")
    assert cmd.rc == 0
    status = json.loads(cmd.stdout)
    assert status['initialized']
    assert not status['sealed']
    assert status['storage_type'] == 'raft'
