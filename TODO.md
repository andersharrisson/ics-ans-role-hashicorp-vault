# TODO list

The storage of the shamir keys is inherently insecure. This becomes tiresome when restarting vault.

###  use transit secret engine (another instance of vault) for auto-unseal at re/startupby
- [ ]  use viable solutions are possible - enabling auto-unseal by using a transit secret engine (another instance of vault) 
  - [ ] reference: https://learn.hashicorp.com/tutorials/vault/autounseal-transit?in=vault/auto-unseal

### include the ability to use consul as a storage backend
- [ ] create a consul role
- [ ] add consul as a storage backend

